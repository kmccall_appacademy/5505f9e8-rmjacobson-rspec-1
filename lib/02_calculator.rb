def add(x,y)
  return x+y
end

def subtract(x,y)
  return x-y
end

def sum(array)
  the_sum = 0
  for i in array do
    the_sum += i
  end

  return the_sum
end
