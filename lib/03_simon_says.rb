def echo(word)
  return word
end

def shout(word)
  return word.upcase
end

def repeat(word,times=2)
  return ("#{word} " * times).rstrip
end

def start_of_word(word, num_chars)
  return word.slice(0..(num_chars-1))
end

def first_word(string)
  return string.split[0]
end

def titleize(string)
  word_list = string.split

  arr = ['a', 'an', 'the', 'or', 'over', 'and']

  word_list = string.split

  for word in word_list
    unless arr.include? word
      word.capitalize!
    end
  end

  word_list[0].capitalize!
  return word_list.join(" ")
end
