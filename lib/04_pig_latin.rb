def translate str


  alpha = ('a'..'z').to_a
  vowels = %w[a e i o u]
  consonants = alpha - vowels

  word_list = str.split
  for word in word_list
    if vowels.include?(word[0])
      word.sub!(word, word + 'ay')
    elsif consonants.include?(word[0]) && consonants.include?(word[1]) && consonants.include?(word[2])
      word.sub!(word, word[3..-1] + word[0..2] + 'ay')
    elsif consonants.include?(word[0]) && consonants.include?(word[1]) && word[2] === 'u'
      word.sub!(word, word[3..-1] + word[0..2] + 'ay')
    elsif consonants.include?(word[0]) && consonants.include?(word[1])
      word.sub!(word, word[2..-1] + word[0..1] + 'ay')
    elsif word[0] === 'q' && word[1] === 'u'
      word.sub!(word, word[2..-1] + word[0..1] + 'ay')
    elsif consonants.include?(word[0]) && consonants.include?(word[1]) && word[2] === 'u'
      word.sub!(word, word[3..-1] + word[0..2] + 'ay')
    elsif consonants.include?(word[0])
      word.sub!(word, word[1..-1] + word[0] + 'ay')
    else
      next
    end
  end
  return word_list.join(" ")

end
